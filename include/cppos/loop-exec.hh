#ifndef LOOP_EXEC_HH_INCLUDED
#define LOOP_EXEC_HH_INCLUDED

#include "execution.hh"
#include <thread>
#include <chrono>

namespace cppos
{
namespace exec = std::execution;
using std::execution::receiver;
using std::execution::sender;

using namespace std::literals;


template <typename Sch>
class LoopExecutor
{
public:
    class Scheduler
    {
        friend class LoopExecutor<Sch>;

        LoopExecutor<Sch> *ctx;

        struct OpBase
        {
            void (*f)(OpBase *);
        };

        template <receiver Recv>
        struct Op : OpBase
        {
            template <receiver R>
            explicit Op(R &&r, LoopExecutor<Sch> *ctx_)
              : ctx(ctx_)
              , rec(std::forward<R>(r))
              , evId(255)
            {
                this->f = [] (OpBase *self)
                {
                    auto me = static_cast<Op *>(self);
                    exec::set_value(Recv(me->rec)); // we need an rvalue
                };
            }

            void setEvent(uint8_t e)
            {
                evId = e;
            }

            friend void tag_invoke(exec::start_t, Op &o) noexcept
            {
                // activate the event
                o.ctx->addTask(&o, o.evId);
            }

            template <sender Snd>
            using SndType = typename std::decay_t<Snd>::value_types;

            //Event?
            LoopExecutor<Sch> *ctx;
            Recv rec;
            uint8_t evId = 255;
        };

    public:
        struct Sender
        {
            typedef void value_types;

            LoopExecutor<Sch> *ctx;
        };

        Scheduler(LoopExecutor<Sch> *ex)
          : ctx(ex)
        {
        }

        template <std::convertible_to<exec::connect_t> T, receiver Recv>
        friend constexpr auto tag_invoke(T , Sender const &s, Recv &&r)
        {
            return Op<std::decay_t<Recv>>(std::forward<Recv>(r), s.ctx);
        }

        template <std::convertible_to<exec::schedule_t> TagT>
        friend constexpr Sender tag_invoke(TagT, Scheduler s)
        {
            return {s.ctx};
        }

        template <typename ComplFoo,
                  std::convertible_to<exec::get_completion_scheduler_t<ComplFoo>> TagT>
        friend Scheduler
        tag_invoke(TagT, Sender s) noexcept
        {
            return {};
        }

        template <std::convertible_to<forEver_t> T, sender Snd>
        friend auto
        tag_invoke(T, Scheduler sch, Snd &&s) noexcept
        {
            auto opState = exec::connect(std::forward<Snd>(s),
                                   impl::VoidRecv());
            exec::start(opState);

            // to keep the opState alive, we must not return!
            while (true)
            {
                std::this_thread::sleep_for(5s);
            }

            return;

        }

        bool operator==(Scheduler const &rhs) const = default;
    };

    LoopExecutor(Sch s)
      : agent(s)
      , ownOp(&ev)
    {
        evMask[ev.id()] = 1;
        ops[ev.id()].op = &ownOp;

        exec::execute(s, [this]
                         {
                             while (true)
                             {
                                 auto msk = select(evMask);
                                 for (size_t i = 0; i != msk.size(); ++i)
                                 {
                                     if (msk[i])
                                     {
                                         msk[i] = 0;
                                         ops[i].op->f(ops[i].op);
                                     }
                                 }
                             }
                         });
    }

    Scheduler scheduler()
    {
        return {this};
    }

    void addTask(Scheduler::OpBase *op, uint8_t evId)
    {
        evMask[evId] = 1;
        ops[evId].op = op;

        ev.unblock_all();
    }

private:
    struct ActivationInfo
    {
        Scheduler::OpBase *op = nullptr;
        // possibly additional info
    };

    struct NewTaskOp : public Scheduler::OpBase
    {
        NewTaskOp(event *e)
          : ev(e)
        {
            this->f = [] (Scheduler::OpBase *self)
            {
                auto me = static_cast<NewTaskOp *>(self);
                me->ev->unblock_all();
            };
        }

        event *ev;
    };

    Sch agent;

    event ev;
    NewTaskOp ownOp;

    std::array<ActivationInfo, sys_traits::max_event_count> ops;
    std::bitset<sys_traits::max_event_count> evMask;
};
} // namespace cppos
#endif /* LOOP_EXEC_HH_INCLUDED */
