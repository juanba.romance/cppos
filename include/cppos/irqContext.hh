#ifndef IRQ_CONTEXT_HH_INCLUDED
#define IRQ_CONTEXT_HH_INCLUDED

#include "port-hardware.hh"

#include "systraits.hh"

#include "execution.hh"
#include <array>

namespace cppos
{
namespace exec = std::execution;

namespace impl
{
struct OpBase
{
    void (*f)(OpBase *);
};

extern std::array<OpBase *, port_traits::irqCnt> isrTable;
} // namespace impl


template <uint8_t IRQ>
class IrqContext
{
public:
    class Scheduler
    {
        template <exec::receiver Recv>
        struct Op : impl::OpBase
        {
            template <exec::receiver R>
            explicit Op(R &&r)
              : rec(std::forward<R>(r))
            {
                this->f = [] (impl::OpBase *self)
                {
                    auto me = static_cast<Op *>(self);
                    std::execution::set_value(Recv(me->rec)); // rvalue
                };
                impl::isrTable[IRQ] = this;
            }

            friend void tag_invoke(exec::start_t, Op &o) noexcept
            {
                port::enableIrq<IRQ>();
            }

            template <exec::sender Snd>
            using SndType = typename std::decay_t<Snd>::value_types;

            Recv rec;
        };

    public:
        struct Sender
        {
            typedef void value_types;
        };

        template <std::convertible_to<exec::connect_t> T, exec::receiver Recv>
        friend constexpr auto tag_invoke(T , Sender const &, Recv &&r)
        {
            return Op<std::decay_t<Recv>>(std::forward<Recv>(r));
        }

        friend constexpr Sender tag_invoke(exec::schedule_t, Scheduler s)
        {
            return {};
        }

        template <typename ComplFoo,
                  std::convertible_to<exec::get_completion_scheduler_t<ComplFoo>> T,
                  typename Err>
        friend Scheduler
        tag_invoke(T, Sender s) noexcept {
            return {};
        }

        template <std::convertible_to<forEver_t> T, exec::sender Snd>
        friend void
        tag_invoke(T, Scheduler, Snd &&s) noexcept
        {
            auto opState = connect(std::forward<Snd>(s),
                                   exec::impl::WaitRecv<void>{});
            exec::start(opState);
        }

        bool operator==(Scheduler const &rhs) const = default;
    };

    Scheduler scheduler()
    {
        return {};
    }

    typedef Scheduler scheduler_type;
};
} // namespace cppos
#endif /* IRQ_CONTEXT_HH_INCLUDED */
