#ifndef _SCHED_HELPER_HH_INCLUDED
#define _SCHED_HELPER_HH_INCLUDED

#include <cppos/scheduler.hh>

namespace cppos::impl
{
extern volatile uint32_t timer_ticks;

class sched_helper
{
public:
    static void yield_for_sleep(uint32_t end);
};
} // namespace cppos::impl
#endif /* _SCHED_HELPER_HH_INCLUDED */
