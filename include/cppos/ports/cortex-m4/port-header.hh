#ifndef CORTEX_M4_PORT_HEADER_HH_INCLUDED
#define CORTEX_M4_PORT_HEADER_HH_INCLUDED

#include <cmsis_gcc.h>

namespace cppos
{
namespace port
{
struct interrupt_lock
{
    [[gnu::always_inline]] interrupt_lock()
      : primask(__get_PRIMASK())
    {
        __disable_irq();
    }
    [[gnu::always_inline]] ~interrupt_lock()
    {
        if (!primask)
        {
            __enable_irq();
        }
    }

    [[gnu::always_inline]] void unlock()
    {
        if (!primask)
        {
            __enable_irq();
        }
        unlocked = true;
    }

    uint32_t primask;
    bool unlocked = false;
};

[[gnu::always_inline]] inline void disable_interrupts()
{
    __disable_irq();
}

[[gnu::always_inline]] inline void enable_interrupts()
{
    __enable_irq();
}

[[gnu::always_inline]] inline bool interrupts_enabled()
{
    return __get_PRIMASK();
}
} // namespace port
} // namespace cppos
#endif /* CORTEX_M4_PORT_HEADER_HH_INCLUDED */
