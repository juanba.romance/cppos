#ifndef AVR_PORT_HARDWARE_HH_INCLUDED
#define AVR_PORT_HARDWARE_HH_INCLUDED

#include <avr/io.h>

namespace cppos
{
namespace port
{
template <uint8_t IRQ>
void enableIrq()
{
    if constexpr (IRQ == TIMER3_COMPA_vect_num)
    {
        TIMSK3 |= _BV(OCIE3A);
    }
    else
    {
        static_assert("unsupported interrupt");
    }
}
} // namespace port
} // namespace cppos
#endif /* AVR_PORT_HARDWARE_HH_INCLUDED */
