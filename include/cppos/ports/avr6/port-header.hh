#ifndef AVR_PORT_HEADER_HH_INCLUDED
#define AVR_PORT_HEADER_HH_INCLUDED

#include <avr/io.h>

namespace cppos
{
namespace port
{
struct interrupt_lock
{
    [[gnu::always_inline]] interrupt_lock()
      : saved_status(SREG)
    {
        __builtin_avr_cli();
    }
    [[gnu::always_inline]] ~interrupt_lock()
    {
        if (!unlocked)
        {
            SREG = saved_status;
        }
    }

    [[gnu::always_inline]] void unlock()
    {
        SREG = saved_status;
        unlocked = true;
    }

    uint8_t saved_status;
    bool unlocked = false;
};

[[gnu::always_inline]] inline void disable_interrupts()
{
    __builtin_avr_cli();
}

[[gnu::always_inline]] inline void enable_interrupts()
{
    __builtin_avr_sei();
}

[[gnu::always_inline]] inline bool interrupts_enabled()
{
    return SREG & SREG_I;
}
} // namespace port
} // namespace cppos
#endif /* AVR_PORT_HEADER_HH_INCLUDED */
