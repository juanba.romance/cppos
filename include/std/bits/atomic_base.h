// <atomic> -*- C++ -*-
#ifndef _ATOMIC_BASE_INCLUDED
#define _ATOMIC_BASE_INCLUDED

#ifdef __AVR
// avr-gcc doesn't have the atomic compiler intrinsics
#include <avr/atomics.hh>
#endif

#include_next <bits/atomic_base.h>
#endif /* _ATOMIC_BASE_INCLUDED */
