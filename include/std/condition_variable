//  -*- C++ -*-
#ifndef _CPPOS_CONDITION_VARIABLE_INCLUDED
#define _CPPOS_CONDITION_VARIABLE_INCLUDED

// for now we do all with disabled interrupts
// so we don't even need atomics
//#include <atomic>
#include <cppos/scheduler.hh>
#include <port-header.hh>
#include <mutex>

namespace std
{
enum class cv_status
{
    no_timeout,
    timeout
};

class condition_variable : public cppos::event
{
public:
    condition_variable() = default;
    condition_variable(condition_variable const &) = delete;
    condition_variable &operator=(condition_variable const &) = delete;

    void notify_one() noexcept
    {
        // this may be called with interrupts disabled
        bool irqs_enabled = cppos::port::interrupts_enabled();
        cppos::port::interrupt_lock l;
        if (blocked.any())
        {
            if (unblock_one() && irqs_enabled)
            {
                cppos::port::yield();
            }
        }
    }

    void notify_all() noexcept
    {
        // this may be called with interrupts disabled
        bool irqs_enabled = cppos::port::interrupts_enabled();
        cppos::port::interrupt_lock l;
        if (blocked.any())
        {
            if (unblock_all() && irqs_enabled)
            {
                cppos::port::yield();
            }
        }
    }

    void wait(std::unique_lock<mutex>& lock);

    template<class Predicate>
    void wait(unique_lock<mutex>& lock, Predicate pred);
    template<class Clock, class Duration>
    cv_status wait_until(unique_lock<mutex>& lock,
                         const chrono::time_point<Clock, Duration>& abs_time);
    template<class Clock, class Duration, class Predicate>
    bool wait_until(unique_lock<mutex>& lock,
                    const chrono::time_point<Clock, Duration>& abs_time,
                    Predicate pred);
    template<class Rep, class Period>
    cv_status wait_for(unique_lock<mutex>& lock,
                       const chrono::duration<Rep, Period>& rel_time);
    template<class Rep, class Period, class Predicate>
    bool wait_for(unique_lock<mutex>& lock,
                  const chrono::duration<Rep, Period>& rel_time,
                  Predicate pred);
};
} // namespace std

#include_next <condition_variable>

#endif /* _CPPOS_CONDITION_VARIABLE_INCLUDED */
