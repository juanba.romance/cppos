#ifndef _FIX_ERRNO_H_INCLUDED
#define _FIX_ERRNO_H_INCLUDED 1
//#include_next <errno.h>
extern "C"
{
extern int errno;
} // extern "C"

// AVR libc defines some values like this:
// #define ENOSYS ((int)(66081697 & 0x7fff))
// the GCC preprocessor can't handle this in expressions like
// #if (!defined ENOSYS || ENOTSUP != ENOSYS)
// so we use final values here

#undef ENOSYS
#define ENOSYS 0x53a1

#undef EINTR
#define EINTR 0x6e4a

#undef ENOERR
#define ENOERR 0x2df2

// these are not used in AVR libc, but C++11 and libstdc++ requires them
// to exist and mainly have different values
#define E2BIG 101
#define EACCES 102
#define EADDRINUSE 103
#define EADDRNOTAVAIL 104
#define EAFNOSUPPORT 105
#define EAGAIN 106
#define EALREADY 107
#define EBADF 108
#define EBUSY 109
#define ECHILD 110
#define ECONNABORTED 111
#define ECONNREFUSED 112
#define ECONNRESET 113
#define EDEADLK 114
#define EDESTADDRREQ 115
#define EDOM 116
#define EEXIST 117
#define EFAULT 118
#define EFBIG 119
#define EHOSTUNREACH 120
#define EILSEQ 121
#define EINPROGRESS 122
#define EINVAL 123
#define EIO 124
#define EISCONN 125
#define EISDIR 126
#define ELOOP 127
#define EMFILE 128
#define EMLINK 129
#define EMSGSIZE 130
#define ENAMETOOLONG 131
#define ENETDOWN 132
#define ENETRESET 133
#define ENETUNREACH 134
#define ENFILE 135
#define ENOBUFS 136
#define ENODEV 137
#define ENOENT 138
#define ENOEXEC 139
#define ENOLCK 140
#define ENOMEM 141
#define ENOMSG 142
#define ENOPROTOOPT 143
#define ENOSPC 144
#define ENOTCONN 145
#define ENOTDIR 146
#define ENOTEMPTY 147
#define ENOTSUP 148
#define ENOTSOCK 149
#define ENOTTY 150
#define ENXIO 151
#define EOPNOTSUPP 152
#define EOVERFLOW 153
#define EPERM 154
#define EPIPE 155
#define EPROTONOSUPPORT 156
#define EPROTOTYPE 157
#define ERANGE 158
#define EROFS 159
#define ESPIPE 160
#define ESRCH 161
#define ETIMEDOUT 162
#define EWOULDBLOCK 163
#define EXDEV 164

#endif /* _FIX_ERRNO_H_INCLUDED */
