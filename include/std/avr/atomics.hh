#ifndef _AVR_ATOMICS_INCLUDED
#define _AVR_ATOMICS_INCLUDED
#pragma GCC system_header

// we need SREG
#include <avr/io.h>

#include <functional>

// we try to get away without all the sized overloads
// we generally ignore the memory order, we're always seq_cst

#define __atomic_load_n(__ptr, __mem_order) \
    __avr_atomic_load_n(__ptr)
#define __atomic_load(__ptr, __ret, __mem_order)      \
    __avr_atomic_load(__ptr, __ret)

#define __atomic_store_n(__ptr, __val, __mem_order)   \
    __avr_atomic_store_n(__ptr, __val)
#define __atomic_store(__ptr, __val, __mem_order)      \
    __avr_atomic_store(__ptr, __val)

#define __atomic_fetch_add(__ptr, __val, __mem_order)   \
    __avr_atomic_fetch_add(__ptr, __val)
#define __atomic_add_fetch(__ptr, __val, __mem_order) \
    __avr_atomic_add_fetch(__ptr, __val)

#define __atomic_fetch_sub(__ptr, __val, __mem_order)   \
    __avr_atomic_fetch_sub(__ptr, __val)
#define __atomic_sub_fetch(__ptr, __val, __mem_order) \
    __avr_atomic_sub_fetch(__ptr, __val)

// helper concepts for 1-byte types
template <typename _T>
concept _IsOneByte = (sizeof(_T) == 1);
template <typename _T>
concept _IsMultipleBytes = (sizeof(_T) > 1);

template <typename _T>
struct _InterruptRestoreHelper;

template <_IsOneByte _T>
struct _InterruptRestoreHelper<_T>
{
};

template <_IsMultipleBytes _T>
struct _InterruptRestoreHelper<_T>
{
    [[gnu::always_inline]] _InterruptRestoreHelper()
      : __saved_status(SREG)
    {
        __builtin_avr_cli();
    }
    [[gnu::always_inline]] ~_InterruptRestoreHelper()
    {
        SREG = __saved_status;
    }

    uint8_t __saved_status;
};

template <typename _T, typename _OT, class __Op>
struct _DelayedOp
{
    [[gnu::always_inline]] _DelayedOp(_T & __obj, _OT __val)
      : __object(__obj),
        __operand(__val)
    {
    }

    [[gnu::always_inline]] ~_DelayedOp()
    {
        __object = (__Op())(__object, __operand);
    }

    _T &__object;
    _OT __operand;
};

template <typename _T>
[[gnu::always_inline]] _T __avr_atomic_load_n(_T const volatile *__ptr)
{
    _InterruptRestoreHelper<_T> __irh;
    return *__ptr;
}

template <typename _T>
[[gnu::always_inline]]
void __avr_atomic_load(_T const volatile *__ptr, _T *__ret)
{
    _InterruptRestoreHelper<_T> __irh;
    *__ret = *__ptr;
}

template <typename _T>
[[gnu::always_inline]]
void __avr_atomic_store_n(_T volatile *__ptr, _T __val)
{
    _InterruptRestoreHelper<_T> __irh;
    *__ptr = __val;
}

template <typename _T>
[[gnu::always_inline]]
void __avr_atomic_store(_T volatile *__ptr, _T const *__val)
{
    _InterruptRestoreHelper<_T> __irh;
    *__ptr = *__val;
}

// atomic_base uses unqualified '1' for ++ and -- (which is probably correct)
template <typename _T, typename _OT>
[[gnu::always_inline]] _T __avr_atomic_add_fetch(_T volatile *__ptr, _OT __val)
{
    _InterruptRestoreHelper<_T> __irh;
    return *__ptr += __val;
}

template <typename _T, typename _OT>
[[gnu::always_inline]] _T __avr_atomic_fetch_add(_T volatile *__ptr, _OT __val)
{
    _InterruptRestoreHelper<_T> __irh;
    _DelayedOp<_T, _OT, std::plus<_T>> __do(*__ptr, __val);
    return *__ptr;
}

template <typename _T, typename _OT>
[[gnu::always_inline]] _T __avr_atomic_sub_fetch(_T volatile *__ptr, _OT __val)
{
    _InterruptRestoreHelper<_T> __irh;
    return *__ptr -= __val;
}

template <typename _T, typename _OT>
[[gnu::always_inline]] _T __avr_atomic_fetch_sub(_T volatile *__ptr, _OT __val)
{
    _InterruptRestoreHelper<_T> __irh;
    _DelayedOp<_T, _OT, std::minus<_T>> __do(*__ptr, __val);
    return *__ptr;
}
#endif /* _AVR_ATOMICS_INCLUDED */
