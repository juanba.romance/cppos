
set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_PROCESSOR avr)
set(CMAKE_CROSSCOMPILING 1)

find_program(AVR_CC ${CROSS_PREFIX}gcc${COMPILER_SUFFIX} REQUIRED)
find_program(AVR_CXX ${CROSS_PREFIX}g++${COMPILER_SUFFIX} REQUIRED)
find_program(AVR_AS ${CROSS_PREFIX}as${COMPILER_SUFFIX} REQUIRED)
find_program(AVR_OBJCOPY ${CROSS_PREFIX}objcopy REQUIRED)
find_program(AVR_SIZE_TOOL ${CROSS_PREFIX}size REQUIRED)
find_program(AVR_OBJDUMP ${CROSS_PREFIX}objdump REQUIRED)

set(CMAKE_C_COMPILER ${AVR_CC})
set(CMAKE_ASM_COMPILER ${AVR_AS})
set(CMAKE_CXX_COMPILER ${AVR_CXX})

add_compile_options(-Wno-volatile -Wno-array-bounds)
add_compile_options(-g -Os)
add_compile_options(-mmcu=${MCU} -DF_CPU=${CLOCK})

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=gnu++2b -fno-rtti -fno-exceptions")

add_link_options(-mmcu=${MCU} -Wl,-Map=target.map)
