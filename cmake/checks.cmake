
# test bit_cast
file(WRITE ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/CMakeTmp/testBit_cast.cc
  "#include <bit>\n"
  "int main()\n"
  "{ return std::bit_cast<int>(1u); }\n")
try_compile(BIT_CAST_OK ${CMAKE_BINARY_DIR}
  ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/CMakeTmp/testBit_cast.cc
  CXX_STANDARD 20
  CXX_STANDARD_REQUIRED TRUE
  OUTPUT_VARIABLE BIT_CAST_OUT
)
if(NOT BIT_CAST_OK)
  message(FATAL_ERROR "bit_cast not supported:\n ${BIT_CAST_OUT}\n")
endif()
