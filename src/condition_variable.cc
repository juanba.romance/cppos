#include <condition_variable>

namespace std
{
void condition_variable::wait(std::unique_lock<mutex>& lock)
{
    cppos::port::disable_interrupts();
    cppos::task_block *me = cppos::exec_context::sys_sched->current_task;
    block(me->idx);
    me->blocked_on = this;
    me->state = cppos::task_state::blocked;
    lock.unlock();
    cppos::port::yield();
    lock.lock();
}
} // namespace cppos
