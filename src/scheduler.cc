#include <cppos/scheduler.hh>

#include <systraits.hh>
#include <cppos/helpers.hh>
#include <cppos/port.hh>
#include <port-header.hh>

#include <cstdint>
#include <array>
#include <cstring>
#include <bit>

#include <cppos/scheduler.tt>

namespace
{
using namespace cppos;

void idle()
{
    while (1)
    {
    }
}

// I don't think this must be extern "C"
// as we don't care about the mangled name
void f_call_wrapper(nullary_function *f)
{
    (*f)();
}

//event selectEv;
std::array<event *, sys_traits::max_event_count> allEvents;
} // unnamed namespace

namespace cppos
{
} // namespace cppos

// for calls from assembler
extern "C"
{
extern std::byte *estack;  // should be provided by linker script

void volatile * volatile * volatile previous_task_sp = nullptr;
void volatile * volatile * volatile current_task_sp = nullptr;
} // extern "C"

namespace cppos
{
namespace impl
{
uint32_t volatile timer_ticks = 0;
} // namespace impl

std::bitset<sys_traits::max_event_count> event::unblocked;
uint8_t event::curEventCnt = 0;

exec_context *exec_context::sys_sched = nullptr;


event::event()
  : id_(curEventCnt++)
{
    allEvents[id_] = this;
}

bool event::unblock_all()
{
    unblocked[id_] = 1;
    uint8_t own_prio = exec_context::sys_sched->current_task->priority;
    bool result = false;
    for_all_blocked_tasks_check_unblock(
        [own_prio, &result] (task_block &t)
        {
            t.state = task_state::ready;
            t.blocked_on = nullptr;

            if (t.priority > own_prio)
            {
                result = true; // yes, unblock
            }
            return true;
        });

    return result;
}

bool event::unblock_one()
{
    unblocked[id_] = 1;
    uint8_t own_prio = exec_context::sys_sched->current_task->priority;
    uint8_t max_prio;
    task_block *to_unblock = nullptr;
    bool result = false;
    for_all_blocked_tasks_check_unblock(
        [&max_prio, &to_unblock] (task_block &t)
        {
            if (to_unblock == nullptr || t.priority > max_prio)
            {
                to_unblock = &t;
                max_prio = t.priority;
            }

            return false; // don't unblock yet
        });

    if (to_unblock != nullptr)
    {
        if (to_unblock->priority > own_prio)
        {
            result = true;
        }

        blocked[to_unblock->idx] = false;
        to_unblock->state = task_state::ready;
        to_unblock->blocked_on = nullptr;
    }

    return result;
}


exec_context::exec_context()
  : assigned_stack_top(estack - sys_traits::main_stack_size)
{
    // idle task should go to end of array, later...
    add(nullary_function(idle), 255, sys_traits::min_stack_size, 0);

    // we only support a single scheduler (at least for now)
    if (sys_sched)
    {
        error_handler();
    }
    sys_sched = this;
}

[[noreturn]] void exec_context::start()
{
    current_task = &(tasks[0]);
    current_run = next_idx;
    schedule();
    // we need to keep the main stack for interrupt handlers (MSP)
    port::initial_switch();
}

void exec_context::schedule()
{
    previous_task_sp = &(current_task->stack);
    size_t cur_size = std::bit_cast<std::byte *>(current_task->stack_end)
        - std::bit_cast<std::byte *>(current_task->stack);
    if (cur_size > current_task->max_stack_size)
    {
        current_task->max_stack_size = cur_size;
    }
    size_t cur_idx = current_task->idx;
    size_t next = cur_idx;
    unsigned prio = current_task->priority;
    uint8_t sub_prio = current_task->last_run;
    if (current_task->state == task_state::blocked)
    {
        next = 0; // idle task
        prio = 0;
        sub_prio = 0;
    }

    for (size_t i = cur_idx + 1; ; ++i)
    {
        if (i == next_idx)
        {
            i = 0;
        }

        if (i == cur_idx)
        {
            break;
        }

        if (tasks[i].state != task_state::ready)
        {
            continue;
        }

        // this is round robin backwards
        // if prios are the same, the last task is chosen
        if (tasks[i].priority < prio)
        {
            continue;
        }

        if (tasks[i].priority == prio)
        {
            uint8_t diff = tasks[i].last_run - sub_prio;
            if (diff < 0x7f)
            {
                continue;
            }
        }

        next = i;
        prio = tasks[i].priority;
        sub_prio = tasks[i].last_run;
    }

    if (cur_idx != next)
    {
        if (current_task->state == task_state::running)
        {
            current_task->state = task_state::ready;
        }
        current_task = &(tasks[next]);
        current_task->state = task_state::running;
    }
    current_task->last_run = current_run++;

    current_task_sp = &(current_task->stack);
}

void exec_context::add(nullary_function &&f,
                       uint8_t id,
                       int stack_size,
                       uint8_t prio)
{
    memset(assigned_stack_top - stack_size, 0xf0, stack_size);

    task_block &new_task = tasks[next_idx];
    new_task.idx = next_idx;
    ++next_idx;

    new_task.init_foo = std::move(f);

    new_task.stack_end = assigned_stack_top;

    assigned_stack_top -= stack_size;

    memcpy(assigned_stack_top - sys_traits::default_stack_guard_size,
           &sys_traits::default_stack_guard_value,
           sys_traits::default_stack_guard_size);

    new_task.stack = port::make_stack(new_task.stack_end,
                                      std::bit_cast<void *>(&error_handler),
                                      std::bit_cast<void *>(&f_call_wrapper),
                                      &new_task.init_foo);

    new_task.priority = prio;
    new_task.id = id;
    // we need distinct values for last_run, the actual value doesn't matter
    new_task.last_run = next_idx;
}

std::bitset<sys_traits::max_event_count>
select(std::bitset<sys_traits::max_event_count> evs)
{
    cppos::port::disable_interrupts();
    cppos::task_block *me = cppos::exec_context::sys_sched->current_task;
    for (size_t i = 0; i != evs.size(); ++i)
    {
        if (evs[i])
        {
            allEvents[i]->block(me->idx);
        }
    }

    //selectEvent.block(me->idx);
    //me->blocked_on = &selectEvent;
    me->state = cppos::task_state::blocked;
    cppos::port::yield();

    return evs & event::unblocked;
}
} // namespace cppos
