// functions required for libstdc++

#include <memory_resource>

extern "C"
{
[[noreturn]] void error_handler();
} // extern "C"

namespace std
{
[[noreturn]] void __throw_length_error(char const *)
{
    error_handler();
}

[[noreturn]] void __throw_logic_error(char const *)
{
    error_handler();
}

[[noreturn]] void __throw_system_error(int)
{
    error_handler();
}

[[noreturn]] void __throw_bad_array_new_length()
{
    error_handler();
}

std::pmr::memory_resource::~memory_resource()
{
}
} // namespace std

void operator delete(void*, unsigned int)
{
}
