// test for Sender/Receiver

#include <cppos/scheduler.hh>
#include <cppos/irqContext.hh>
#include <cppos/loop-exec.hh>
#include <cppos/execution.hh>

#include <execution>
#include <chrono>
#include <cstdint>
#include <atomic>

#include <outStream.hh>

#include <avr/io.h>

#include <cppos/scheduler.tt>


using namespace std::literals;
namespace exec = std::execution;

uint8_t volatile *uint32ToHex(uint32_t val, uint8_t volatile *start);
uint8_t volatile *uint16ToHex(uint16_t val, uint8_t volatile *start);
uint8_t volatile *uint8ToHex(uint8_t val, uint8_t volatile *start);

namespace
{

uint8_t execCtxBuf[sizeof(cppos::exec_context)];

uint8_t buf[80] = "Here's the board\r\n";

std::atomic<uint32_t> counters[3];

uint16_t timerTask()
{
    static uint16_t counter = 0;

    return counter++;
}

void printTask(uint16_t val)
{
    auto t = std::chrono::steady_clock::now().time_since_epoch();
    auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(t);
    uint8_t volatile *end = uint32ToHex(ms.count(), buf);
    *(end++) = ' ';

    end = uint16ToHex(val, end);
    *(end++) = '\r';
    *(end++) = '\n';
    dbgOut.write(buf, end - buf);

    return;
}

cppos::scheduler<20, 300, 20> *appSched = nullptr;
constexpr uint8_t demoTimerIrq = TIMER3_COMPA_vect_num;

void timerIrqSetup()
{
    OCR3A = 32000;
    TCCR3B = 0x5; // prescale /1024
    TCNT3 = 0;
}

void ctrlTask()
{

    cppos::IrqContext<demoTimerIrq> iCtx;
    exec::scheduler auto sch = iCtx.scheduler();
    cppos::LoopExecutor appCtx(*appSched);

    timerIrqSetup();
#if 0
    exec::sender auto isr = schedule(sch);
    exec::sender auto q1 = then(isr, timerTask);
    exec::sender auto app = bufferedTransfer<4>(q1, appCtx.scheduler());
    exec::sender auto fin = then(app, printTask);
    sync_wait(fin);
#else
    exec::schedule(sch)
        | exec::then(timerTask)
        | cppos::bufferedTransfer<4>(appCtx.scheduler())
        | exec::then(printTask)
        | cppos::forEver();
    // forEver is wrong here as we switch to a different agent
    // it should be ensure_started
    // but then we have a lifetime issue for the operation state
    // so forEver must never return
#endif

    SIMOUT('Y'); // never reached
}

void busyTask(void *taskVar)
{
    std::atomic<uint32_t> count = 0; // doesn't need to be atomic
    std::atomic<uint32_t> *taskCnt
        = static_cast<std::atomic<uint32_t> *>(taskVar);
    uint8_t id = taskCnt - counters;
    SIMOUT('0' + id);

    while (true)
    {
        ++count;
        if (count == 1'000'000)
        {
            SIMOUT('0' + id);
            count = 0;
            ++(*taskCnt);
        }
    }
}
} // unnamed namespace

extern "C"
{
void error_handler()
{
    while (true)
    {
    }
}
} // extern "C"

int main( void )
{
    auto ctx = new (execCtxBuf) cppos::exec_context;

    /* exec::scheduler */ auto schForTimer = cppos::scheduler<20, 300, 20>(ctx);
    appSched = &schForTimer;

    // we have a bug if the task is a function pointer
    exec::execute(cppos::scheduler<1, 300, 20>(ctx), [] { ctrlTask(); });

    exec::execute(cppos::scheduler<10>(ctx), [] { busyTask(counters); });
    exec::execute(cppos::scheduler<11>(ctx), [] { busyTask(counters + 1); });
    exec::execute(cppos::scheduler<12>(ctx), [] { busyTask(counters + 2); });

    // Start everything
    ctx->start();

    /* we should never get here! */
    return 0;
}
