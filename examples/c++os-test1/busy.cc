
#include <cstdint>

extern uint32_t volatile * volatile cntPtr[3];
extern uint32_t volatile counters[3];

void busyTask(void volatile *taskVar)
{
    volatile uint32_t count = 0;
    uint32_t volatile *taskCnt = static_cast<uint32_t volatile *>(taskVar);
    uint8_t id = taskCnt - counters;
    cntPtr[id] = &count;

    while (true)
    {
        ++count;
        if (count == 1'000'000)
        {
            count = 0;
            ++(*taskCnt);
        }
    }
}
