// very simple scheduling example

#include <cppos/scheduler.hh>

#include <execution>
#include <chrono>
#include <thread>
#include <charconv>

#include <outStream.hh>

#include <ex-helpers.hh>

#include <cppos/scheduler.tt>

using namespace std::literals;


void ledFlashTask();
void busyTask(void volatile *);

uint32_t volatile *cntPtr[3] = {nullptr};
uint32_t volatile counters[3] = {0};

namespace
{
uint8_t execCtxBuf[sizeof(cppos::exec_context)];

char buf[80] = "Here's the board\r\n";

void printTask()
{
    dbgOut.write(buf, 18);
    auto bufEnd = buf + 80;

    auto printPeriod = 10s;

    while (1)
    {
        std::this_thread::sleep_for(printPeriod);
        auto t = std::chrono::steady_clock::now().time_since_epoch();
        auto sec = std::chrono::duration_cast<std::chrono::seconds>(t);
        char *end = buf;
        end = std::to_chars(end, bufEnd, sec.count()).ptr;
        *(end++) = ' ';
        for (int i = 0; i != 3; ++i)
        {
            end = std::to_chars(end, bufEnd, counters[i]).ptr;
            *(end++) = ' ';
            uint32_t volatile *p = cntPtr[i];
            end = std::to_chars(end, bufEnd, *p).ptr;
            *(end++) = ' ';
        }
        *(end++) = '\r';
        *(end++) = '\n';
        dbgOut.write(buf, end - buf);
    }
}

struct task_info
{
    void volatile *used_stack;
    uint8_t idx = 255; // index in array
};

std::array<task_info, 6> tasks;
cppos::exec_context *ctx = nullptr;
} // unnamed namespace

namespace cppos
{
void volatile *debug_handle()
{
    std::byte volatile *p;

    for (size_t i = 0; i != 6; ++i)
    {
        tasks[i].idx = i;
        if (i != 5)
        {
            p = static_cast<std::byte volatile *>(ctx->tasks[i+1].stack_end);
        }
        else
        {
            p = static_cast<std::byte volatile *>(ctx->assigned_stack_top);
        }
        p += sys_traits::default_stack_guard_size;
        for ( ; p != ctx->tasks[i].stack_end; ++p)
        {
            if (*p != std::byte(0xf0))
            {
                break;
            }
        }
        tasks[i].used_stack = p;
    }

    p += 10; // dummy for breakpoint

    return p;
}
} // namespace cppos

extern "C"
{
void error_handler()
{
    cppos::debug_handle();

    while (true)
    {
    }
}
} // extern "C"

int main( void )
{
    system_setup();
    namespace exec = std::execution;
    ctx = new (execCtxBuf) cppos::exec_context;

    // we have a bug if the task is a function pointer
    exec::execute(cppos::scheduler<1, 440>(ctx), [] { ledFlashTask(); });
    exec::execute(cppos::scheduler<2, 440>(ctx), [] { printTask(); });
    exec::execute(cppos::scheduler<10>(ctx), [] { busyTask(counters); });
    exec::execute(cppos::scheduler<11>(ctx), [] { busyTask(counters + 1); });
    exec::execute(cppos::scheduler<12>(ctx), [] { busyTask(counters + 2); });

    // Start everything
    ctx->start();

    /* we should never get here! */
    return 0;
}
