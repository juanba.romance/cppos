#ifndef GPIO2_HH_SEEN
#define GPIO2_HH_SEEN

#include <avr/io.h>

#if !_SFR_ASM_COMPAT
constexpr uint16_t port2addr(uint8_t volatile &p)
{
	return reinterpret_cast<uintptr_t>(&p);
}

// C++ rules say that several things are not allowed for constexpr functions
// so we need to use a macro here :-(

// 'PORTB' from avr/io.h expands essentially to *(uint8_t *)(0x25)
// we actually need the 0x25
//#define UNREG8(x) (reinterpret_cast<uintptr_t>(&x))
#define UNREG8(x) ((uintptr_t)(&x))
#endif

constexpr uint8_t bitVal(uint8_t bit)
{
    return 1 << bit;
}

template <uintptr_t port, uint8_t bit>
class GpioOut
{
public:
	GpioOut(bool initState)
	{
		set(initState);
		*ddrReg() |= bitVal(bit);
	}

	void set(bool state)
	{
		if (state)
		{
			*portReg() |= bitVal(bit);
		} else {
			*portReg() &= ~_BV(bit);
		}
	}

    void toggle()
    {
        *portReg() ^= bitVal(bit);
    }

private:
	constexpr uint8_t volatile *portReg() {
		return reinterpret_cast<uint8_t volatile *>(port);
	}
	constexpr uint8_t volatile *ddrReg() {
		return portReg() - 1;
	}
};
#endif /* GPIO2_HH_SEEN */
