// simple debug output via UART

#include "outStream.hh"

#include <avr/io.h>
#include <string.h>

#ifndef UDR
#define UDR UDR0
#define UCSRB UCSR0B
#define UBRRL UBRR0L
#define UBRRH UBRR0H
#define UDRIE UDRIE0
#define TXCIE TXCIE0
#define TXEN TXEN0
#endif


void OutStream::reset()
{
    // UART settings
    UCSRB = 0;
    uint16_t brVal = (F_CPU / (16UL * uartBaud)) - 1;
    UBRRL = uint8_t(brVal & 0xff);
    UBRRH = uint8_t(brVal >> 8);
    // default UCSRC is 8n1, which is just fine
}

OutStream::OutStream()
{
    reset();
}

OutStream::~OutStream()
{
    UCSRB = 0;
}

void OutStream::sendNext()
{
    if (txCurrent != txLen)
    {
        UDR = buf[txCurrent++];
    }
    else
    {
        if (txLen != 0)
        {
            UCSRB &= ~_BV(UDRIE);
            UCSRB |= _BV(TXCIE);
        }
        else
        {
            txState = idle;
        }
        txCurrent = txLen = 0;
    }
}

void OutStream::write(void const *data, uint16_t len)
{
    if (txState == txBusy)
    {
        return;
    }
    txState = txBusy;

    memcpy(buf, (void *)data, len);

    txCurrent = 0;
    txLen = len;
    sendNext();

    // now we start the transmitter
    UCSRB |= _BV(TXEN);

    // enable TX empty irq
    UCSRB |= _BV(UDRIE);
}

bool OutStream::done() const
{
    return txState == txDone;
}

OutStream dbgOut;

extern "C"
{
//ISR(USART_UDRE_vect)
[[gnu::signal,gnu::used,gnu::externally_visible]]
void USART_UDRE_vect()
{
    dbgOut.sendNext();
}

//ISR(USART_TXCIE_vect)
[[gnu::signal,gnu::used,gnu::externally_visible]]
void USART_TX_vect()
{
    UCSRB &= ~_BV(TXCIE);
    dbgOut.txState = OutStream::txDone;
}
} // extern "C"
