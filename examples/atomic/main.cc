// simple atomics testing

#include <cppos/scheduler.hh>

#include <execution>
#include <chrono>
#include <thread>
#include <cstdint>
#include <atomic>

#include <outStream.hh>

#include <cppos/scheduler.tt>

#include <ex-helpers.hh>

using namespace std::literals;


uint8_t volatile *uint32ToHex(uint32_t val, uint8_t volatile *start);
uint8_t volatile *uint8ToHex(uint8_t val, uint8_t volatile *start);

std::atomic<uint32_t> *cntPtr[3] = {nullptr};
std::atomic<uint32_t> counters[3] = {0};


namespace
{
uint8_t execCtxBuf[sizeof(cppos::exec_context)];

enum class IoState : uint32_t
{
    idle,
    txBusy,
    txDone,
    rxBusy,
    rxDone,
};

uint8_t buf[80] = "Here's the board\r\n";

void printTask()
{
    dbgOut.write(buf, 18);

    auto printPeriod = 10s;

    while (1)
    {
        std::this_thread::sleep_for(printPeriod);
        auto t = std::chrono::steady_clock::now().time_since_epoch();
        auto sec = std::chrono::duration_cast<std::chrono::seconds>(t);
        uint8_t volatile *end = uint32ToHex(sec.count(), buf);
        *(end++) = ' ';
        for (int i = 0; i != 2; ++i)
        {
            end = uint32ToHex(counters[i], end);
            *(end++) = ' ';
            uint32_t localCnt = *(cntPtr[i]);
            end = uint32ToHex(localCnt, end);
            *(end++) = ' ';
        }
        *(end++) = '\r';
        *(end++) = '\n';
        dbgOut.write(buf, end - buf);
    }
}

void busyTask(void *taskVar)
{
    std::atomic<uint32_t> count = 0;
    std::atomic<uint32_t> *taskCnt
        = static_cast<std::atomic<uint32_t> *>(taskVar);
    uint8_t id = taskCnt - counters;
    cntPtr[id] = &count;

    while (true)
    {
        ++count;
        if (count == 1'000'000)
        {
            count = 0;
            ++(*taskCnt);
        }
    }
}

void busyTask8Bit()
{
    std::atomic<uint8_t> count = 0;
    std::atomic<uint32_t> *taskCnt = &(counters[2]);

    while (true)
    {
        ++count;
        if (count == 0)
        {
            uint32_t extCnt = ++(*taskCnt);
        }
    }
}

struct task_info
{
    void volatile *used_stack;
    uint8_t idx = 255; // index in array
};

std::array<task_info, 6> tasks;
cppos::exec_context *ctx = nullptr;
} // unnamed namespace

namespace cppos
{
void volatile *debug_handle()
{
    std::byte volatile *p;

    for (size_t i = 0; i != 6; ++i)
    {
        tasks[i].idx = i;
        if (i != 5)
        {
            p = static_cast<std::byte volatile *>(ctx->tasks[i+1].stack_end);
        }
        else
        {
            p = static_cast<std::byte volatile *>(ctx->assigned_stack_top);
        }
        p += sys_traits::default_stack_guard_size;
        for ( ; p != ctx->tasks[i].stack_end; ++p)
        {
            if (*p != std::byte(0xf0))
            {
                break;
            }
        }
        tasks[i].used_stack = p;
    }

    p += 10; // dummy for breakpoint

    return p;
}
} // namespace cppos

extern "C"
{
void error_handler()
{
    cppos::debug_handle();

    while (true)
    {
    }
}
} // extern "C"

int main( void )
{
    system_setup();
    namespace exec = std::execution;
    ctx = new (execCtxBuf) cppos::exec_context;

    // we have a bug if the task is a function pointer
    exec::execute(cppos::scheduler<2, 440>(ctx), [] { printTask(); });
    exec::execute(cppos::scheduler<10>(ctx), [] { busyTask(counters); });
    exec::execute(cppos::scheduler<11>(ctx), [] { busyTask(counters + 1); });
    exec::execute(cppos::scheduler<12>(ctx), [] { busyTask8Bit(); });

    // Start everything
    ctx->start();

    /* we should never get here! */
    return 0;
}
