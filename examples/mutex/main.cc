// very simple scheduling example using mutexes

#include <cppos/scheduler.hh>

#include <execution>
#include <chrono>
#include <thread>
#include <mutex>
#include <cstdint>
#include <atomic>

#include <outStream.hh>

#include <cppos/scheduler.tt>

#include <ex-helpers.hh>

using namespace std::literals;


uint8_t volatile *uint32ToHex(uint32_t val, uint8_t volatile *start);
uint8_t volatile *uint8ToHex(uint8_t val, uint8_t volatile *start);

namespace
{
uint8_t execCtxBuf[sizeof(cppos::exec_context)];

uint8_t buf[80] = "Here's the board\r\n";

std::atomic<uint32_t> counters[3];
std::mutex startMtx[3];
std::atomic<bool> startFlag[3];

void printTask()
{
    dbgOut.write(buf, 18);

    auto printPeriod = 10s;

    while (1)
    {
        std::this_thread::sleep_for(printPeriod);
        auto t = std::chrono::steady_clock::now().time_since_epoch();
        auto sec = std::chrono::duration_cast<std::chrono::seconds>(t);
        uint8_t volatile *end = uint32ToHex(sec.count(), buf);
        *(end++) = ' ';
        for (int i = 0; i != 3; ++i)
        {
            end = uint32ToHex(counters[i], end);
            *(end++) = ' ';
        }
        *(end++) = '\r';
        *(end++) = '\n';
        dbgOut.write(buf, end - buf);
    }
}

void ctrlTask()
{
    // lock the mutexes
    for (uint8_t i = 0; i != 3; ++i)
    {
        startMtx[i].lock();
    }

    // set start flags
    for (uint8_t i = 0; i != 3; ++i)
    {
        startFlag[i] = true;
    }

    std::this_thread::sleep_for(2s);

    // start the first task
    startMtx[0].unlock();

    std::this_thread::sleep_for(2s);

    // start the second task
    startMtx[1].unlock();

    std::this_thread::sleep_for(2s);

    // start the third task
    startMtx[2].unlock();

    // we don't return and simply stop by waiting on a mutex that's locked
    startMtx[0].lock();
}

void busyTask(void *taskVar)
{
    std::atomic<uint32_t> count = 0; // doesn't need to be atomic
    std::atomic<uint32_t> *taskCnt
        = static_cast<std::atomic<uint32_t> *>(taskVar);
    uint8_t id = taskCnt - counters;

    // wait for flag
    while (!startFlag[id])
    {
        std::this_thread::sleep_for(1ms);
    }

    // wait for mutex
    startMtx[id].lock();

    while (true)
    {
        ++count;
        if (count == 1'000'000)
        {
            count = 0;
            ++(*taskCnt);
        }
    }
}
} // unnamed namespace

extern "C"
{
void error_handler()
{
    while (true)
    {
    }
}
} // extern "C"

int main( void )
{
    system_setup();
    namespace exec = std::execution;
    auto ctx = new (execCtxBuf) cppos::exec_context;

    for (uint8_t i = 0; i != 3; ++i)
    {
        startFlag[i] = false;
    }

    // we have a bug if the task is a function pointer
    exec::execute(cppos::scheduler<1, 300, 20>(ctx), [] { ctrlTask(); });
    exec::execute(cppos::scheduler<2, 440>(ctx), [] { printTask(); });
    exec::execute(cppos::scheduler<10>(ctx), [] { busyTask(counters); });
    exec::execute(cppos::scheduler<11>(ctx), [] { busyTask(counters + 1); });
    exec::execute(cppos::scheduler<12>(ctx), [] { busyTask(counters + 2); });

    // Start everything
    ctx->start();

    /* we should never get here! */
    return 0;
}
